#pragma once

#include "GSEGlobals.h"

class GSEObject
{
public:
	GSEObject();
	~GSEObject();

	void Update(float elapsedTimeInSec, GSEUpdateParams* param);

	void SetPosition(float x, float y, float depth);
	void SetSize(float sx, float sy);

	void SetRelPosition(float x, float y, float depth);
	void GetRelPosition(float* x, float* y, float* depth);

	void GetPosition(float* x, float* y, float* depth);
	void GetSize(float* sx, float* sy);

	void SetVel(float x, float y);
	void GetVel(float* x, float* y);

	void SetAcc(float x, float y);
	void GetAcc(float* x, float* y);

	void SetMass(float x);
	void GetMass(float* x);

	void SetType(GSEObjectType type);
	void GetType(GSEObjectType* type);

	void SetState(GSEObjectState state);
	void GetState(GSEObjectState* state);

	void SetParentID(int parentID);
	int GetParentID();

	void SetLifeTime(float lifeTime);
	float GetLifeTime();

	void SetLife(float life);
	float GetLife();

	void SetApplyPhysics(bool bPhy);
	bool GetApplyPhysics();

	void SetCoolTime(float coolTime);

	void ResetRemainingCoolTime();
	float GetRemainingCoolTime();

	void SetStickToParent(bool bStick);
	bool GetStickToParent();

	void SetTextureID(int id);
	int GetTextureID();

private:
	float m_PositionX, m_PositionY;
	float m_RelPositionX, m_RelPositionY;
	float m_Depth;
	float m_RelDepth;
	float m_SizeX, m_SizeY;
	float m_VelX, m_VelY;
	float m_AccX, m_AccY;
	float m_Mass;
	int m_Parent;	//hero ID, npc ID
	float m_LifeTime;
	float m_Life;
	bool m_ApplyPhysics;
	float m_CoolTime;
	float m_RemainingCoolTime;
	bool m_StickToParent;
	int m_TextureID;

	GSEObjectState m_State;
	GSEObjectType m_Type;
};

