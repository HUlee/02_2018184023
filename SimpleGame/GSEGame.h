#pragma once
#include "Renderer.h"
#include "GSEObject.h"
#include "GSEGlobals.h"
#include "Sound.h"

class GSEGame
{
public:
	GSEGame();
	~GSEGame();

	void RenderScene();
	int AddObject(float x, float y, float depth,
		float sx, float sy,
		float velX, float velY,
		float accX, float accY,
		float mass);
	void DeleteObject(int index);
	void Update(float elapsedTimeInSec, GSEInputs* inputs);
	void SceneTitle();
	void SceneStage1();
	void SceneStage2();
	void SceneEnding();
	void MakeHero();
	void MakeEnemy();
	void MakeMapStage();

private:
	bool AABBCollision(GSEObject* a, GSEObject* b);
	bool ProcessCollision(GSEObject* a, GSEObject* b);
	void AdjustPosition(GSEObject* a, GSEObject* b);
	void DoGarbageCollect();

	Renderer* m_renderer = NULL;
	Sound* m_Sound = NULL;
	GSEObject* m_Objects[GSE_MAX_OBJECTS];

	int m_HeroID = -1;
	int m_EnemyID = -1;
	int m_SharpID = -1;

	int m_HeroTexture = -1;
	int m_EnemyTexture = -1;

	int m_SharpTextureUp = -1;
	int m_SharpTextureDown = -1;

	int m_BrickTexture = -1;
	int m_Brick2Texture = -1;

	int m_SpriteTexture = -1;

	int m_BGTexture = -1;

	int m_SwordTexture = -1;

	int m_NoticeTexture = -1;

	int m_EndingTexture = -1;

	int m_BGSound = -1;
	int m_EndSound = -1;
	int m_JumpSound = -1;
	int m_SwordSound = -1;
	int m_CollisionSound = -1;
};

