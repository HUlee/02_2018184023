#pragma once

#define GSE_MAX_OBJECTS 1000
#define GSE_GRAVITY 9.8f
#define GSE_WINDOWSIZE_X 800	
#define GSE_WINDOWSIZE_Y 400

typedef struct GSEInputs
{
	bool ARROW_UP;
	bool ARROW_DOWN;
	bool ARROW_LEFT;
	bool ARROW_RIGHT;

	bool KEY_W;
	bool KEY_A;
	bool KEY_S;
	bool KEY_D;

	bool KEY_F1;
	bool KEY_F2;
	bool KEY_F3;
	bool KEY_F4;
	bool KEY_F5;

	bool GAME_END = false;
};

typedef struct GSEUpdateParams
{
	float forceX;
	float forceY;
};

enum GSEObjectType {
	TYPE_HERO,
	TYPE_MOVABLE,
	TYPE_FIXED,
	TYPE_BULLET, // -> �߰�
	TYPE_SWORD,
	TYPE_ENEMY,
	TYPE_SHARP,
	TYPE_PORTAL
};

enum GSEObjectState {
	STATE_GROUND,
	STATE_FALLING,
	STATE_FLY,
	STATE_SWIMMING,
	STATE_TRAP
};