#include "stdafx.h"
#include "GSEGame.h"
#include "math.h"
#include <conio.h>

int s_Num = 1;

extern GSEInputs g_inputs;

GSEGame::GSEGame()
{
	//Renderer initialize
	m_renderer = new Renderer(GSE_WINDOWSIZE_X, GSE_WINDOWSIZE_Y);
	m_Sound = new Sound();

	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		m_Objects[i] = NULL;
	}

	// 이미지 뒤집는 건 알아서 하셈
	//m_SpriteTexture = m_renderer->GenPngTexture("spriteSheet.png");
	m_HeroTexture = m_renderer->GenPngTexture("Player00.png");	// png 불러오는 거 -> 재사용 가능
	m_EnemyTexture = m_renderer->GenPngTexture("Monster00.png");
	m_BGTexture = m_renderer->GenPngTexture("Background.png");
	m_BrickTexture = m_renderer->GenPngTexture("brick.png");	// png 불러오는 거 -> 재사용 가능
	m_Brick2Texture = m_renderer->GenPngTexture("brick2.png");
	m_SharpTextureUp = m_renderer->GenPngTexture("Sharp0.png");	// png 불러오는 거 -> 재사용 가능
	m_SharpTextureDown = m_renderer->GenPngTexture("Sharp1.png");	// png 불러오는 거 -> 재사용 가능
	m_NoticeTexture = m_renderer->GenPngTexture("TitleNotice.png");
	m_EndingTexture = m_renderer->GenPngTexture("Ending.png");
	m_SwordTexture = m_renderer->GenPngTexture("Knife.png");

	if (s_Num == 1) {
		SceneTitle();
	}
	if (s_Num == 2) {
		SceneStage1();
	}
	if (s_Num == 3) {
		SceneStage2();
	}
	if (s_Num == 4) {
		SceneEnding();
	}

	m_BGSound = m_Sound->CreateBGSound("BGM.wav");
	m_EndSound = m_Sound->CreateShortSound("Ending.wav");
	m_JumpSound = m_Sound->CreateShortSound("Jump.wav");
	m_SwordSound = m_Sound->CreateShortSound("Sword.wav");
	m_CollisionSound = m_Sound->CreateShortSound("Collision.wav");

	m_Sound->PlayBGSound(m_BGSound, true, 1.f);
}

GSEGame::~GSEGame()
{
	//Renderer delete
}

void GSEGame::SceneTitle()
{
	int floor = AddObject(0, -0.25, 0, 10, 5, 0, 0, 0, 0, 20);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_NoticeTexture);
}

void GSEGame::SceneStage1()
{
	MakeHero();
	MakeMapStage();
}

void GSEGame::SceneStage2()
{
	MakeHero();
	MakeEnemy();
	MakeMapStage();
}

void GSEGame::SceneEnding()
{
	int floor = AddObject(0, -0.25, 0, 10, 5, 0, 0, 0, 0, 20);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_EndingTexture);

	m_Sound->PlayShortSound(m_EndSound, false, 1.f);
	m_Sound->DeleteBGSound(m_BGSound);
}

void GSEGame::MakeHero()
{
	//Create Hero
	m_HeroID = AddObject(-20, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
	m_Objects[m_HeroID]->SetType(GSEObjectType::TYPE_HERO);
	m_Objects[m_HeroID]->SetApplyPhysics(true);
	m_Objects[m_HeroID]->SetLife(100000000.f);
	m_Objects[m_HeroID]->SetLifeTime(100000000.f);
	m_Objects[m_HeroID]->SetTextureID(m_HeroTexture);
}

void GSEGame::MakeEnemy()
{
	m_EnemyID = AddObject(-18, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
	m_Objects[m_EnemyID]->SetType(GSEObjectType::TYPE_ENEMY);
	m_Objects[m_EnemyID]->SetState(GSEObjectState::STATE_GROUND);
	m_Objects[m_EnemyID]->SetApplyPhysics(true);
	m_Objects[m_EnemyID]->SetLife(100000000.f);
	m_Objects[m_EnemyID]->SetLifeTime(100000000.f);
	m_Objects[m_EnemyID]->SetTextureID(m_EnemyTexture);

	m_EnemyID = AddObject(-14.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
	m_Objects[m_EnemyID]->SetType(GSEObjectType::TYPE_ENEMY);
	m_Objects[m_EnemyID]->SetState(GSEObjectState::STATE_GROUND);
	m_Objects[m_EnemyID]->SetApplyPhysics(true);
	m_Objects[m_EnemyID]->SetLife(100000000.f);
	m_Objects[m_EnemyID]->SetLifeTime(100000000.f);
	m_Objects[m_EnemyID]->SetTextureID(m_EnemyTexture);

	m_EnemyID = AddObject(-7.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
	m_Objects[m_EnemyID]->SetType(GSEObjectType::TYPE_ENEMY);
	m_Objects[m_EnemyID]->SetState(GSEObjectState::STATE_GROUND);
	m_Objects[m_EnemyID]->SetApplyPhysics(true);
	m_Objects[m_EnemyID]->SetLife(100000000.f);
	m_Objects[m_EnemyID]->SetLifeTime(100000000.f);
	m_Objects[m_EnemyID]->SetTextureID(m_EnemyTexture);

	m_EnemyID = AddObject(-4, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
	m_Objects[m_EnemyID]->SetType(GSEObjectType::TYPE_ENEMY);
	m_Objects[m_EnemyID]->SetState(GSEObjectState::STATE_GROUND);
	m_Objects[m_EnemyID]->SetApplyPhysics(true);
	m_Objects[m_EnemyID]->SetLife(100000000.f);
	m_Objects[m_EnemyID]->SetLifeTime(100000000.f);
	m_Objects[m_EnemyID]->SetTextureID(m_EnemyTexture);

	m_EnemyID = AddObject(3, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
	m_Objects[m_EnemyID]->SetType(GSEObjectType::TYPE_ENEMY);
	m_Objects[m_EnemyID]->SetState(GSEObjectState::STATE_GROUND);
	m_Objects[m_EnemyID]->SetApplyPhysics(true);
	m_Objects[m_EnemyID]->SetLife(100000000.f);
	m_Objects[m_EnemyID]->SetLifeTime(100000000.f);
	m_Objects[m_EnemyID]->SetTextureID(m_EnemyTexture);

	m_EnemyID = AddObject(10, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
	m_Objects[m_EnemyID]->SetType(GSEObjectType::TYPE_ENEMY);
	m_Objects[m_EnemyID]->SetState(GSEObjectState::STATE_GROUND);
	m_Objects[m_EnemyID]->SetApplyPhysics(true);
	m_Objects[m_EnemyID]->SetLife(100000000.f);
	m_Objects[m_EnemyID]->SetLifeTime(100000000.f);
	m_Objects[m_EnemyID]->SetTextureID(m_EnemyTexture);

}

void GSEGame::MakeMapStage()
{
	//MapLine
	for (int i = -50; i < 51; ++i)
	{
		int floor = AddObject(i * 0.5, 1.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);
	}
	for (int i = -50; i < 51; ++i)
	{
		int floor = AddObject(i * 0.5, -1.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);
	}

	//Start
	for (int i = -50; i < -42; ++i)
	{
		int floor = AddObject(i * 0.5, 1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		floor = AddObject(i * 0.5, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		floor = AddObject(i * 0.5, -0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		floor = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);
	}

	//Portal
	for (int i = 43; i < 44; ++i)
	{
		int floor = AddObject(i * 0.5, 1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_PORTAL);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_Brick2Texture);

		floor = AddObject(i * 0.5, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_PORTAL);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_Brick2Texture);

		floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_PORTAL);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_Brick2Texture);

		floor = AddObject(i * 0.5, -0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_PORTAL);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_Brick2Texture);

		floor = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_PORTAL);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_Brick2Texture);
	}

	//End
	for (int i = 44; i < 50; ++i)
	{
		int floor = AddObject(i * 0.5, 1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		floor = AddObject(i * 0.5, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		floor = AddObject(i * 0.5, -0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		floor = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);
	}

	//MapWall
	for (int i = -37; i < -34; ++i)
	{
		int floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		//MapSharp
		m_SharpID = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureUp);
	}
	for (int i = -30; i < -27; ++i)
	{
		//MapSharp
		m_SharpID = AddObject(i * 0.5, 1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureDown);
	}
	for (int i = -23; i < -20; ++i)
	{
		int floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		//MapSharp
		m_SharpID = AddObject(i * 0.5, 0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureUp);
	}
	for (int i = -16; i < -13; ++i)
	{
		//MapSharp
		m_SharpID = AddObject(i * 0.5, 1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureDown);
	}
	for (int i = -9; i < -6; ++i)
	{
		int floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		//MapSharp
		m_SharpID = AddObject(i * 0.5, -0.5, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureDown);

		//MapSharp
		m_SharpID = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureUp);
	}
	for (int i = -2; i < 1; ++i)
	{
		//MapSharp
		m_SharpID = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureUp);
	}
	for (int i = 5; i < 8; ++i)
	{
		int floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		//MapSharp
		m_SharpID = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureUp);
	}
	for (int i = 12; i < 15; ++i)
	{
		//MapSharp
		m_SharpID = AddObject(i * 0.5, 1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureDown);
	}
	for (int i = 19; i < 22; ++i)
	{
		int floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		//MapSharp
		m_SharpID = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureUp);
	}
	for (int i = 26; i < 29; ++i)
	{
		//MapSharp
		m_SharpID = AddObject(i * 0.5, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureUp);
	}
	for (int i = 33; i < 36; ++i)
	{
		int floor = AddObject(i * 0.5, 0, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_Objects[floor]->SetState(GSEObjectState::STATE_GROUND);
		m_Objects[floor]->SetApplyPhysics(true);
		m_Objects[floor]->SetLife(100000000.f);
		m_Objects[floor]->SetLifeTime(100000000.f);
		m_Objects[floor]->SetTextureID(m_BrickTexture);

		//MapSharp
		m_SharpID = AddObject(i * 0.5, 1, 0, 0.5, 0.5, 0, 0, 0, 0, 20);
		m_Objects[m_SharpID]->SetType(GSEObjectType::TYPE_SHARP);
		m_Objects[m_SharpID]->SetApplyPhysics(true);
		m_Objects[m_SharpID]->SetLife(100000000.f);
		m_Objects[m_SharpID]->SetLifeTime(100000000.f);
		m_Objects[m_SharpID]->SetTextureID(m_SharpTextureDown);
	}
}

void GSEGame::Update(float elapsedTimeInSec, GSEInputs* inputs)
{
	//do garbage collecting
	DoGarbageCollect();

	GSEUpdateParams othersParam;
	GSEUpdateParams heroParam;
	memset(&othersParam, 0, sizeof(GSEUpdateParams));
	memset(&heroParam, 0, sizeof(GSEUpdateParams));

	///////////////////////////////////////////////////////////////////

	//calc force
	float forceAmount = 250.f;
	if (inputs->KEY_W)
	{
		heroParam.forceY += 20 * forceAmount;
		m_Sound->PlayShortSound(m_JumpSound, false, 0.5f);
	}
	if (inputs->KEY_A)
	{
		heroParam.forceX -= forceAmount;
	}
	if (inputs->KEY_S)
	{
		heroParam.forceY -= forceAmount;
		m_Sound->PlayShortSound(m_JumpSound, false, 0.5f);
	}
	if (inputs->KEY_D)
	{
		heroParam.forceX += forceAmount;
	}

	if (inputs->KEY_F1)
	{
		s_Num = 1;
		g_inputs.KEY_F1 = false;
		for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
			m_Objects[i] = NULL;
		SceneTitle();
		m_Sound->PlayBGSound(m_BGSound, true, 1.f);
		cout << s_Num << endl;
	}
	if (inputs->KEY_F2)
	{
		s_Num = 2;
		g_inputs.KEY_F2 = false;
		for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
			m_Objects[i] = NULL;
		SceneStage1();
		cout << s_Num << endl;
	}
	if (inputs->KEY_F3)
	{
		s_Num = 3;
		g_inputs.KEY_F3 = false;
		for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
			m_Objects[i] = NULL;
		SceneStage2();
		cout << s_Num << endl;
	}
	if (inputs->KEY_F4)
	{
		s_Num = 4;
		g_inputs.KEY_F4 = false;
		for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
			m_Objects[i] = NULL;
		SceneEnding();
		cout << s_Num << endl;
	}

	//sword
	float swordPosX = 0.f;
	float swordPosY = 0.f;

	if (inputs->ARROW_LEFT) swordPosX += -1.f;
	if (inputs->ARROW_RIGHT) swordPosX += 1.f;
	if (inputs->ARROW_DOWN) swordPosY += -1.f;
	if (inputs->ARROW_UP) swordPosY += 1.f;
	float swordDirSize = sqrtf(swordPosX * swordPosX + swordPosY * swordPosY);
	if (swordDirSize > 0.f)
	{
		float norDirX = swordPosX / swordDirSize;
		float norDirY = swordPosY / swordDirSize;

		float aX, aY, asX, asY;
		float bX, bY, bsX, bsY;
		float temp;

		m_Objects[m_HeroID]->GetPosition(&aX, &aY, &temp);
		m_Objects[m_HeroID]->GetSize(&asX, &asY);

		if (m_Objects[m_HeroID]->GetRemainingCoolTime() < 0.f)
		{
			int swordID = AddObject(0.f, 0.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);
			m_Objects[swordID]->SetParentID(m_HeroID);
			m_Objects[swordID]->SetType(GSEObjectType::TYPE_SWORD);
			m_Objects[swordID]->SetRelPosition(norDirX, norDirY, 0.f);
			m_Objects[swordID]->SetStickToParent(true);
			m_Objects[swordID]->SetLife(100.f);
			m_Objects[swordID]->SetLifeTime(0.3f);
			m_Objects[m_HeroID]->ResetRemainingCoolTime();
			m_Objects[swordID]->SetTextureID(m_SwordTexture);

			m_Sound->PlayShortSound(m_SwordSound, false, 1.f);
		}
	}

	//Processing collision
	bool isCollide[GSE_MAX_OBJECTS];
	memset(isCollide, 0, sizeof(bool) * GSE_MAX_OBJECTS);
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		for (int j = i + 1; j < GSE_MAX_OBJECTS; j++)
		{
			if (m_Objects[i] != NULL && m_Objects[j] != NULL)
			{
				GSEObjectType aType, bType;
				m_Objects[i]->GetType(&aType);
				m_Objects[j]->GetType(&bType);

				if (m_Objects[i]->GetApplyPhysics() && m_Objects[j]->GetApplyPhysics())
				{
					bool collide = ProcessCollision(m_Objects[i], m_Objects[j]);
					if (collide)
					{
						isCollide[i] = true;
						isCollide[j] = true;
					}
				}
				else if (aType == GSEObjectType::TYPE_SWORD && bType == GSEObjectType::TYPE_ENEMY)
				{
					bool collide = ProcessCollision(m_Objects[i], m_Objects[j]);
					if (collide)
					{
						isCollide[i] = true;
						isCollide[j] = true;

						m_Objects[j] = NULL;
						cout << "적과 충돌" << endl;
					}
				}
				else if(bType == GSEObjectType::TYPE_SWORD && aType == GSEObjectType::TYPE_ENEMY) {
					bool collide = ProcessCollision(m_Objects[i], m_Objects[j]);
					if (collide)
					{
						isCollide[i] = true;
						isCollide[j] = true;

						m_Objects[i] = NULL;
						cout << "적과 충돌" << endl;
					}
				}
			}
		}
	}

	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			if (!isCollide[i])
				m_Objects[i]->SetState(GSEObjectState::STATE_FALLING);
		}
	}

	//Update All Objects
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			if (i == m_HeroID)
			{
				m_Objects[i]->Update(elapsedTimeInSec, &heroParam);
			}
			else
			{
				if (m_Objects[i]->GetStickToParent())
				{
					float posX, posY, depth;
					float relPosX, relPosY, relDepth;
					int parentID = m_Objects[i]->GetParentID();
					m_Objects[parentID]->GetPosition(&posX, &posY, &depth);
					m_Objects[i]->GetRelPosition(&relPosX, &relPosY, &relDepth);
					m_Objects[i]->SetPosition(posX + relPosX, posY + relPosY, depth + relDepth);
					m_Objects[i]->Update(elapsedTimeInSec, &othersParam);
				}
				else
				{
					m_Objects[i]->Update(elapsedTimeInSec, &othersParam);
				}
			}
		}
	}

	float x, y, z;
	m_Objects[m_HeroID]->GetPosition(&x, &y, &z);
	m_renderer->SetCameraPos(x*100, y*100);

	///////////////////////////////////////////////////////////////////
}

bool GSEGame::ProcessCollision(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType, bType;
	a->GetType(&aType);
	b->GetType(&bType);

	bool isCollide = AABBCollision(a, b);
	if (isCollide)
	{
		//do something
		if (aType == GSEObjectType::TYPE_FIXED || bType == GSEObjectType::TYPE_FIXED)
		{
			a->SetState(GSEObjectState::STATE_GROUND);
			b->SetState(GSEObjectState::STATE_GROUND);
		}
		if ((aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_PORTAL) || (bType == GSEObjectType::TYPE_HERO && aType == GSEObjectType::TYPE_PORTAL))
		{
			cout << "포탈 이동" << endl;
			if (s_Num == 2)
			{
				s_Num == 3;
				for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
					m_Objects[i] = NULL;
				SceneStage2();
			}
			if (s_Num == 3)
			{
				s_Num == 4;
				for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
					m_Objects[i] = NULL;
				SceneEnding();
			}
		}
		if((aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_ENEMY)|| (bType == GSEObjectType::TYPE_HERO && aType == GSEObjectType::TYPE_ENEMY)
			|| (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_SHARP) || (bType == GSEObjectType::TYPE_HERO && aType == GSEObjectType::TYPE_SHARP))
		{
			if (s_Num == 2)
			{
				for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
					m_Objects[i] = NULL;
				SceneStage1();
			}
			if (s_Num == 3)
			{
				for (int i = 0; i < GSE_MAX_OBJECTS; ++i)
					m_Objects[i] = NULL;
				SceneStage2();
			}
		}
	}
	return isCollide;
}

bool GSEGame::AABBCollision(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;

	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	float pX, pY;
	if (a->GetParentID() != -1)
	{
		a->GetRelPosition(&aX, &aY, &temp);
		m_Objects[a->GetParentID()]->GetPosition(&pX, &pY, &temp);
		aX += pX;
		aY += pY;
	}
	if (b->GetParentID() != -1)
	{
		b->GetRelPosition(&bX, &bY, &temp);
		m_Objects[b->GetParentID()]->GetPosition(&pX, &pY, &temp);
		bX += pX;
		bY += pY;
	}

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if (aMinX > bMaxX) // || fabs(aMinX-bMaxX)<FLT_EPSILON
	{
		return false;
	}
	if (aMaxX < bMinX)
	{
		return false;
	}
	if (aMinY > bMaxY)
	{
		return false;
	}
	if (aMaxY < bMinY)
	{
		return false;
	}

	AdjustPosition(a, b);
	return true;
}

void GSEGame::AdjustPosition(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;

	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if ((aType == GSEObjectType::TYPE_MOVABLE || aType == GSEObjectType::TYPE_HERO)
		&&
		bType == GSEObjectType::TYPE_FIXED)
	{
		if (aMaxY > bMaxY)
		{
			aY = aY + (bMaxY - aMinY);

			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);
		}
		else
		{
			aY = aY - (aMaxY - bMinY);

			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);
		}
	}
	else if (
		(bType == GSEObjectType::TYPE_MOVABLE || bType == GSEObjectType::TYPE_HERO)
		&&
		(aType == GSEObjectType::TYPE_FIXED)
		)
	{
		if (!(bMaxY > aMaxY && bMinY < aMinY))
		{
			if (bMaxY > aMaxY)
			{
				bY = bY + (aMaxY - bMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(vx, 0.f);
			}
			else
			{
				bY = bY - (bMaxY - aMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(vx, 0.f);
			}
		}
	}
}

void GSEGame::DoGarbageCollect()
{
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			float life = m_Objects[i]->GetLife();
			float lifeTime = m_Objects[i]->GetLifeTime();
			if (life < 0.f || lifeTime < 0.f)
			{
				DeleteObject(i);
			}
		}
	}
}

int temp = 0;

void GSEGame::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.2f, 0.3f, 0.6f, 1.0f);

	// Renderer Test
	//m_renderer->DrawSolidRect(0, 0, 0, 100, 1, 0, 1, 1);
	m_renderer->DrawGround(0, 0, 0, 5000, 800, 1, 1, 1, 1, 1, m_BGTexture);
	if(s_Num == 1){}

	//Draw All Objects
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			float x, y, depth;
			m_Objects[i]->GetPosition(&x, &y, &depth);
			float sx, sy;
			m_Objects[i]->GetSize(&sx, &sy);

			int textureID = m_Objects[i]->GetTextureID();

			//meter to pixel
			x = x * 100.f;
			y = y * 100.f;
			sx = sx * 100.f;
			sy = sy * 100.f;

			if (textureID < 0)
			{
				m_renderer->DrawSolidRect(x, y, depth, sx, sy, 0.f, 1, 0, 1, 1);
			}
			else
			{
				m_renderer->DrawTextureRect(
					x, y, depth,
					sx, sy, 1.f,
					1.f, 1.f, 1.f, 1.f,
					textureID);
				//m_renderer->DrawTextureRectAnim(
				//	x, y, depth,
				//	sx, sy, 1.f,
				//	1.f, 1.f, 1.f, 1.f,
				//	textureID,
				//	12, 12, temp, 0);
				//temp++;
				//temp = temp % 12;
			}
		}
	}
}

int GSEGame::AddObject(float x, float y, float depth,
	float sx, float sy,
	float velX, float velY,
	float accX, float accY,
	float mass)
{
	//find empty slot
	int index = -1;
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
		{
			index = i;
			break;
		}
	}

	if (index < 0)
	{
		std::cout << "No empty object slot.. " << std::endl;
		return -1;
	}

	m_Objects[index] = new GSEObject();
	m_Objects[index]->SetPosition(x, y, depth);
	m_Objects[index]->SetSize(sx, sy);
	m_Objects[index]->SetVel(velX, velY);
	m_Objects[index]->SetAcc(accX, accY);
	m_Objects[index]->SetMass(mass);

	return index;
}

void GSEGame::DeleteObject(int index)
{
	if (m_Objects[index] != NULL)
	{
		delete m_Objects[index];
		m_Objects[index] = NULL;
	}
	else
	{
		std::cout << "Try to delete NULL object : " << index << std::endl;
	}
}
