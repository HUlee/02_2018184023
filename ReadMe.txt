1. 시작 시 렌더 버그로 인해 타이틀이 안 보일 경우가 있습니다.
2. W, A, S, D로 이동할 수 있습니다.
3. F2를 누르면 게임이 시작되고 맵을 이동하며 장애물을 피해 맵 오른쪽 끝 포탈을 타면 스테이지가 넘어갑니다.
4. 스테이지1은 장애물만, 스테이지2는 몬스터까지 조재하여 방향키를 통해 몬스터를 없앨 수 있습니다.
5. 스테이지2 맵 포탈을 타면 엔딩이 나옵니다.
6. F1 : 타이틀 / F2 : 스테이지1 / F3 : 스테이지2 / F4 : 엔딩